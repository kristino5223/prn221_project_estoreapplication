﻿const chatButton = document.querySelector('.chatbox__button');
const chatContent = document.querySelector('.chatbox__support');
const icons = {
    isClicked: '<img src="./assets/img/Chat_Icon_rs.png"/>',
    isNotClicked: '<img src="./assets/img/Chat_Icon_rs.png"/>'
}
const chatbox = new InteractiveChatbox(chatButton, chatContent, icons);
chatbox.display();
chatbox.toggleIcon(false, chatButton);